﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Illustrator;

namespace Illest
{
    public class Ill_PathItem
    {
        private PathItem pathItem;
        public Ill_PathItem(PathItem item)
        {
            pathItem = item;
        }



        public double strokeWidth
        {
            get
            {
                return pathItem.StrokeWidth;
            }
            set
            {
                pathItem.StrokeWidth = value;
            }
        }

        public bool IsClosed
        {
            get
            {
                return pathItem.Closed;
            }
        }

        public bool hasStroke
        {
            get
            {
                return pathItem.Stroked;
            }

            set
            {
                pathItem.Stroked = value;
            }
        }

        public bool hasFill
        {
            get
            {
                return pathItem.Filled;
            }

            set
            {
                pathItem.Filled = value;
            }
        }

        public System.Drawing.Color fillColor
        {
            get
            {
                return colorFromIllColor(pathItem.FillColor);
            }

            set
            {

                pathItem.FillColor = IllColFromColor(value);
                pathItem.Opacity = ((double)value.A / (double)255) * 100;
            }
        }

        public System.Drawing.Color strokeColor
        {
            get
            {
                return colorFromIllColor(pathItem.StrokeColor);
            }

            set
            {

                pathItem.StrokeColor = IllColFromColor(value);
            }
        }

        public List<Ill_PathPoint> PathPoints
        {
            get
            {
                var pointList = new List<Ill_PathPoint>();
                foreach (PathPoint p in pathItem.PathPoints)
                {
                    pointList.Add(new Ill_PathPoint(p));
                }
                return pointList;
            }
        }

        private static RGBColor IllColFromColor(System.Drawing.Color value)
        {
           
            var RGBCol = new RGBColor();
            RGBCol.Red = value.R;
            RGBCol.Green = value.G;
            RGBCol.Blue = value.B;
            return RGBCol;
        }

        private System.Drawing.Color colorFromIllColor(object illColor)
        {
            try
            {
                byte opacity = 255;
                opacity = Convert.ToByte(Math.Min(pathItem.Opacity / 100.0 * 255, 255));

                RGBColor rgbCol = illColor as RGBColor;
                if (rgbCol != null)
                {
                    System.Drawing.Color c = System.Drawing.Color.FromArgb(opacity, (int)rgbCol.Red, (int)rgbCol.Green, (int)rgbCol.Blue);
                    return c;
                }
                CMYKColor cmykColor = illColor as CMYKColor;
                if (cmykColor != null)
                {
                    object[] componentsIn = new object[] { cmykColor.Cyan, cmykColor.Magenta, cmykColor.Yellow, cmykColor.Black };
                    object[] components = cmykColor.Application.ConvertSampleColor(AiImageColorSpace.aiImageCMYK, componentsIn, AiImageColorSpace.aiImageRGB, AiColorConvertPurpose.aiDefaultPurpose);
                    System.Drawing.Color c = System.Drawing.Color.FromArgb(opacity, Convert.ToInt32(components[0]), Convert.ToInt32(components[1]), Convert.ToInt32(components[2]));
                    return c;
                }
            }
            catch
            {

            }

            return System.Drawing.Color.Transparent;
        }

    }
}
